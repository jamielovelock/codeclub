# README #

Codeclub files needed to support learning to code. 

### What is this repository for? ###

* Stores vagrantfile
* Contains sample code to assist in lessons

### How do I get set up? ###

* Download and install [virtualbox](https://www.virtualbox.org/)  
* Download and install [vagrant](https://www.vagrantup.com/)
* Download and install [Git](https://git-scm.com/)
* Check out this repository. git clone git@bitbucket.org:jamielovelock/codeclub.git
* edit your hosts file (/etc/hosts/) and add '192.168.56.101  codeclub.dev www.codeclub.dev'
* cd into repo folder
* run: vagrant up
* Edit files in repofolder/codeclub
